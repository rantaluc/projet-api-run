<style>
	body {
		background-color:black;
		color: green;
}
</style>
<h1>Voici toutes les infos:</h1>
<img src = "https://media.tenor.com/CgGUXc-LDc4AAAAM/hacker-pc.gif" >
<?php 
    $utilisateur = get_current_user();
    echo "<p>Nom d'utilisateur actuel : $utilisateur</p>";

    $distribution = shell_exec('lsb_release -ds');
    echo "<p>Distribution utilisée : $distribution</p>";

    $versionNoyau = shell_exec('uname -r');
    echo "<p>Version du noyau Linux utilisée : $versionNoyau</p>";

    $ramDisponible = shell_exec('free -h | awk \'/^Mem/ {print $7}\'');
    $ramTotale = shell_exec('free -h | awk \'/^Mem/ {print $2}\'');
    echo "<p>Taille de la mémoire RAM disponible : $ramDisponible</p>";
    echo "<p>Taille de la mémoire RAM totale : $ramTotale</p>";

    $stockageDisponible = shell_exec('df -h --output=avail / | awk \'/[0-9]+/ {print $1}\'');
    $stockageTotale = shell_exec('df -h --output=size / | awk \'/[0-9]+/ {print $1}\'');
    echo "<p>Taille de stockage disponible : $stockageDisponible</p>";
    echo "<p>Taille de stockage totale : $stockageTotale</p>";
?>


